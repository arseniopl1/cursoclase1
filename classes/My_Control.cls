/**
 *@Name: My_Control
 *@Owner: Arsenio
*/
public with sharing class My_Control {     //Aqui respeta las reglas
//public whithout sharing class My_Control //Aqui no respeta las reglas
    public static String MyMethod() {
        patito__c pat = new patito__c();
        pat=[select id from patito__c limit 1];
        return String.valueOf(pat.Id);
    }
    @AuraEnabled
    public static String CrearPatito (String algo){
        patito__c pat = new patito__c(name =algo);
        insert pat;
        return String.valueOf(pat.Id);
		//insert new patito__c(name =algo);
       // Database.insert(pat);
       // Database.insert(new patito__c(name ='HOla'));
    }
    @AuraEnabled
    public static void ModificarPatito(String Ids, String neName) {
        patito__c pat = [select id, name from patito__c where id=: Ids];
        pat.Name= neName;
        update pat;
    }
    @AuraEnabled
    public static void EliminarPatito(String ids) {
        System.debug('ids'+ids);
        patito__c pat = [select id, name from patito__c where id=:ids];
        delete pat;
    }
    public static void ModificaPatito(String name) {
        insert new patito__c (Name =name);
    }
    public static List<patito__c> ConsultaAllPatos (){
        return [Select id from patito__c];
    }
    public static void DuenoPatito (String owPAtito) {
        insert new patito__c(name='hola',OwnerId=owPatito);
    }
    public static patito__c CampoXpatito(){
        return [select id, name, OwnerID, Owner.Name, Owner.IsActive, Owner.ProfileId, Owner.Profile.Name from patito__c limit 1];
    }
}