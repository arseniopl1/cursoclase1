({
	helperMethod : function(component, event) {
		var action = component.get("c.CrearPatito");
        action.setParams({"algo":component.get("v.vala2")});
        action.setCallback(this,function(response) {
            let state = response.getState();
            if(state==="SUCCESS"){
                component.set("v.vala",response.getReturnValue());
            }
            else{
                alert("Algo fallo");
            }
        });
        $A.enqueueAction(action);
	},
    helperMethod2 : function(component, event) {
		var action = component.get("c.ModificarPatito");
        action.setParams({"Ids":component.get("v.vala"),"neName":component.get("v.vala3")});
        action.setCallback(this,function(response) {
            let state = response.getState();
            if(state==="SUCCESS"){
                alert("El registro se Actualizo correctamente");
            }
            else{
                alert("Algo fallo");
            }
        });
        $A.enqueueAction(action);
	},
 	helperMethod3 : function(component, event) {
				var action = component.get("c.EliminarPatito");
        action.setParams({"ids":component.get("v.vala")});
        action.setCallback(this,function(response) {
            let state = response.getState();
            if(state==="SUCCESS"){
                alert("El registro se Elimino correctamente");
                component.set("v.vala","");
                component.set("v.vala2","");
                component.set("v.vala3","");
            }
            else{
                alert("Algo fallo");
            }
        });
        $A.enqueueAction(action);
	}
})