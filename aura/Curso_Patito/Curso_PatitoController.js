({
	MyAction1 : function(component, event, helper) {
        helper.helperMethod(component, event);
	},
    MyAction2 : function(component, event, helper) {
        helper.helperMethod2(component, event);
	},
	MyAction3 : function(component, event, helper) {
        helper.helperMethod3(component, event);
	}
})